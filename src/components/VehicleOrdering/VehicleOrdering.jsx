import React, {Component} from 'react';
import glamorous from 'glamorous';

import CustomSelect from '../shared/form/CustomSelect';

import style from './style'

const Div = glamorous.div();
const I = glamorous.i();
const H1 = glamorous.h1();
const Button = glamorous.button();

class VehicleOrdering extends Component {

    render() {

        const orderOptions = [
            {label: 'Merk oplopend', value: 'BASC'},
            {label: 'Merk aflopend', value: 'BDESC'},
        ];

        const viewOrderOptions = [
            {label: `Lijst met foto's `, value: 'with-pictures'},
            {label: `Lijst zonder foto's`, value: 'no-pictures'},
        ];

        return (
            <Div css={style.container}>

                <Div css={style.orderingWrapper}>
                    <Div css={style.select}>
                        <I css={style.orderingIcon} className="fa fa-sort"/>
                        <CustomSelect placeholder="Sorteren"
                                      options={orderOptions}
                                      defaultValue={'BASC'}
                                      className={'vehicles_ordering'}/>
                    </Div>
                    <Div css={style.select}>
                        <I css={style.orderingIcon} className="fa fa-list"/>
                        <CustomSelect placeholder="Sorteren"
                                      options={viewOrderOptions}
                                      defaultValue={'with-pictures'}
                                      className={'vehicles_ordering'}/>
                    </Div>


                    <Div css={style.more}>
                        <I css={style.moreIcon} className="fa fa-ellipsis-v"/>
                        <Button css={style.moreButton}>
                            Meer
                        </Button>
                    </Div>
                </Div>
            </Div>
        )
    }
}

export default VehicleOrdering;
