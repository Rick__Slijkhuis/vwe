export default {
    container: props => ({
        display     : 'flex',
        width       : '100%',
        textAlign   : 'right',
        marginBottom: 20,

        [`@media ${props.theme.mediaQueries.tablet}`]: {
            height: 38,
        },
    }),

    heading: props => ({
        fontSize  : '26px',
        lineHeight: '36px',
        padding   : 0,
        margin    : 0,
    }),

    orderingWrapper: props => ({
        flex: 1
    }),

    select: props => ({
        display  : 'block',
        boxSizing: 'border-box',
        position : 'relative',
        width    : '100%',
        padding  : '0 0 0 30px',
        margin   : '0 auto 10px',

        [`@media ${props.theme.mediaQueries.tablet}`]: {
            display    : 'inline-block',
            maxWidth   : 280,
            marginRight: 20,
        },
    }),

    more: props => ({
        display   : 'block',
        boxSizing : 'border-box',
        position  : 'relative',
        width     : '100%',
        height    : '36px',
        lineHeight: '36px',
        fontSize  : '14px',
        margin    : '0 auto 10px',

        [`@media ${props.theme.mediaQueries.tablet}`]: {
            display: 'inline-block',
            padding: '0 0 0 30px',
            width  : 'auto',
            margin : 0,
        },
    }),

    moreButton: props => ({
        boxSizing      : 'border-box',
        width          : '100%',
        textAlign      : 'center',
        lineHeight     : '36px',
        padding        : '0 20px',
        border         : 'none',
        borderRadius   : 0,
        outline        : 'none',
        cursor         : 'pointer',
        borderBottom   : `1px solid ${props.theme.colors.lightGrey}`,
        backgroundColor: props.theme.colors.turquoise,
        color          : props.theme.colors.white,

        '&:focus': {
            outline: 'none',
        },

        [`@media ${props.theme.mediaQueries.tablet}`]: {
            width: 'auto'
        },
    }),

    orderingIcon: props => ({
        position: 'absolute',
        top     : 12,
        left    : 6
    }),

    moreIcon: props => ({
        display : 'none',
        position: 'absolute',
        top     : 12,
        left    : 6,

        [`@media ${props.theme.mediaQueries.tablet}`]: {
            display: 'block'
        },
    }),
}

