export default {
    header: props => ({
        height      : 90,
        borderBottom: `1px solid ${props.theme.colors.lightGrey}`,
        padding     : '0 30px'
    }),

    body: props => ({
        maxWidth : 1200,
        boxSizing: 'border-box',
        margin   : '0 auto'
    }),

    logo: props => ({
        width : 90,
        height: 90
    })
}

