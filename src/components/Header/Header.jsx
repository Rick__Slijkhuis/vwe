import React, {Component} from 'react';
import glamorous from 'glamorous';

import style from './style'

const Header = glamorous.header();
const Div = glamorous.div();
const Img = glamorous.img();

class MainHeader extends Component {

    render() {

        return (
            <Header css={style.header}>
                <Div css={style.body}>
                    <Img css={style.logo} src="/images/logo.svg"/>
                </Div>
            </Header>
        )
    }
}

export default MainHeader;
