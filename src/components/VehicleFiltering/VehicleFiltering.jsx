import React, {Component} from 'react';
import glamorous from 'glamorous';

import CustomSelect from '../shared/form/CustomSelect';
import TextInput from '../shared/form/TextInput';

import style from './style'

const Div = glamorous.div();
const Form = glamorous.form();
const H2 = glamorous.h2();
const H3 = glamorous.h3();
const I = glamorous.i();

class VehicleFiltering extends Component {

    /**
     * Remove doubles from the given key
     *
     * @param key
     * @param fromMake
     * @returns {Array}
     */
    removeDoubles(key, fromMake = null) {

        let vehicles = this.props.vehicles;
        let options = [{label: 'Toon alles', value: ''}];

        if(fromMake) {
            vehicles = this.props.vehicles.filter(vehicle => {
                 return vehicle.get('make') === fromMake;
            });
        }

        vehicles.map((vehicle) => {
            if(options.indexOf(vehicle.get(key)) === -1) {
                options.push({label: vehicle.get(key).toLowerCase(), value: vehicle.get(key)});
            }
        });

        return options;
    }

    render() {

        return (
            <Div css={style.container}>
                <Form css={style.form}>
                    <H2 css={style.heading}>
                        <I css={style.headingIcon} className="fa fa-filter"/>
                        Verfijnen
                    </H2>

                    <TextInput css={style.licensePlate} onChange={e => this.props.updateVehiclesList('licencePlate', e.target.value)} placeholder="XX-XX-XX"/>

                    <H3 css={style.subHeading}>Merk / model</H3>

                    <CustomSelect options={this.removeDoubles('make')}
                                  value={this.props.filters.get('make')}
                                  onChange={value => this.props.updateVehiclesList('make', value)}
                                  placeholder="Kies een merk"/>
                    <CustomSelect options={this.removeDoubles('model', this.props.filters.get('make'))}
                                  disabled={!this.props.filters.get('make')}
                                  value={this.props.filters.get('model')}
                                  onChange={value => this.props.updateVehiclesList('model', value)}
                                  placeholder="Kies een model"/>
                </Form>
            </Div>
        )
    }
}

export default VehicleFiltering;
