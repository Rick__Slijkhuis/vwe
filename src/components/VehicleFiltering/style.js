export default {
    container: props => ({
        display     : 'none',
        marginBottom: 20,

        [`@media ${props.theme.mediaQueries.laptop}`]: {
            display   : 'block',
            width     : 280,
            paddingTop: 58,
        },
    }),

    heading: props => ({
        boxSizing : 'border-box',
        padding   : '0 20px',
        lineHeight: '70px',
        margin    : 0,
        color     : props.theme.colors.white,
    }),

    subHeading: props => ({
        boxSizing : 'border-box',
        padding   : '0 20px',
        lineHeight: '60px',
        margin    : 0,
        color     : props.theme.colors.white,
    }),

    headingIcon: props => ({
        marginRight: 10,
    }),

    form: props => ({
        backgroundColor: props.theme.colors.darkGrey,
        paddingBottom  : 40,
    }),

    licensePlate: props => ({
        ...props.theme.fonts.bodyBold,
        backgroundColor: props.theme.colors.licensePlate,
        border         : 'none',
        lineHeight     : '50px',
        padding        : 0,
        textAlign      : 'center',
        textTransform  : 'uppercase',
    }),
}

