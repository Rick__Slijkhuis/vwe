export default {

    container: props => ({

        [`@media ${props.theme.mediaQueries.tablet}`]: {
            flex       : props.flex ? 1 : '0 1 auto',
            paddingLeft: !props.home ? 20 : 0,
        },

        [`@media ${props.theme.mediaQueries.laptop}`]: {
            paddingLeft: 20,
        },
    })
}

