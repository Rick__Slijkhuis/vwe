export default {

    sidebar: props => ({

        [`@media ${props.theme.mediaQueries.tablet}`]: {
            maxWidth: 320,
        },
    }),
}

