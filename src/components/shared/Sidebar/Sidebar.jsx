import React, {Component} from 'react';
import glamorous from 'glamorous';

import style from './style'

const Div = glamorous.div();

class Sidebar extends Component {

    render() {

        return (
            <Div css={themeProps => style.sidebar({...themeProps, ...this.props})}>
                {this.props.children}
            </Div>
        )
    }
}

export default Sidebar;
