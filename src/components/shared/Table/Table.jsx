import React, {Component} from 'react';
import glamorous from 'glamorous';

import style from './style'

const Table = glamorous.table();
const Tr = glamorous.tr();
const Td = glamorous.td();

class Sidebar extends Component {

    render() {

        return (
            <Table css={style.table}>
                <tbody>
                {this.props.items.map((item, key) => (
                    <Tr key={key}>
                        <Td css={style.label}>
                            {item.key}
                        </Td>
                        <Td css={style.body}>
                            {item.value}
                        </Td>
                    </Tr>
                ))}
                </tbody>
            </Table>
        )
    }
}

export default Sidebar;
