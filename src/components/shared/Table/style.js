export default {

    table: props => ({
        display      : 'inline-block',
        width        : '280px',
        fontSize     : '11px',
        lineHeight   : '17px',
        verticalAlign: 'top',

        [`@media ${props.theme.mediaQueries.tablet}`]: {
            fontSize     : '14px',
            lineHeight   : '24px',
        },
    }),

    label: props => ({
        ...props.theme.fonts.bodyBold,
        boxSizing   : 'border-box',
        minWidth    : 130,
        paddingRight: 10,

        [`@media ${props.theme.mediaQueries.tablet}`]: {
            minWidth    : 165,
        },
    }),

    body: props => ({}),
}

