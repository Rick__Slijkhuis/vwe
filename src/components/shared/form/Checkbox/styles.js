export default {
    main: {
        display  : 'flex',
        textAlign: 'left',
        padding  : '10px 0',
    },

    checkbox: (props) => ({
        display        : 'inline-block',
        width          : '18px',
        height         : '18px',
        position       : 'relative',
        border         : `1px solid ${props.theme.colors.grey}`,
        borderRadius   : '4px',
        backgroundColor: props.theme.colors.greyLight,
        marginRight    : '10px',
        flexShrink     : '0',
    }),

    icon: (props) => ({
        position: 'absolute',
        left    : '4px',
        top     : '1px',
        fontSize: '11px',
        color   : props.theme.colors.purple,
        opacity : '0',
    }),

    input: (props) => ({
        display : 'inline-block',
        opacity : '0',
        position: 'absolute',

        '&:checked + .checkbox'            : {
            borderColor: props.theme.colors.purple
        },
        '&:checked + .checkbox .icon-tick ': {
            opacity: '1',
        },
    }),

    label: {
        display: 'inline-block',
    },
}
