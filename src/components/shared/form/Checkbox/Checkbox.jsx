import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';

import styles from './styles';

const Span = glamorous.span();
const Div = glamorous.div();
const Input = glamorous.input();
const Label = glamorous.label();

const Checkbox = props => (
    <Label css={styles.main}>
        <Input css={styles.input} type={props.inputType} name={props.name} checked={props.checked} value={props.value} onChange={props.onChange} className={props.className} />
        <Div className="checkbox" css={styles.checkbox}>
            <Span className="icon-tick" css={styles.icon}/>
        </Div>
        <Span css={styles.label}>{props.children}</Span>
    </Label>
);

Checkbox.propTypes = {
    step     : PropTypes.number,
    name     : PropTypes.string,
    checked  : PropTypes.bool,
    onChange : PropTypes.func,
    inputType: PropTypes.oneOf(['checkbox', 'radio']),
    className: PropTypes.string,
};

Checkbox.defaultProps = {
    inputType: 'checkbox',
};

export default Checkbox;
