import glamorous from 'glamorous';
import PropTypes from 'prop-types';

const TextArea = glamorous.textarea((props) => ({
    display: 'inline-block',
    boxSizing: 'border-box',
    width: '100%',
    padding: '10px',
    border: `1px solid ${props.theme.colors.grey}`,
    backgroundColor: props.theme.colors.white,
    color: props.theme.colors.greyDark,
    fontSize: '16px',
    textTransform: 'none',
    cursor: 'auto',
    margin: 0,
    minHeight: '100px',

    '&:focus': {
        borderColor: props.theme.colors.turquoise
    },
    
}), (props) => (props.css));

TextArea.propTypes = {
    css: PropTypes.object,
};

TextArea.defaultProps = {
    css: {},
};

export default TextArea;

