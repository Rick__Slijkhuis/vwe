export default {
    component: (props) => ({
        display        : 'inline-block',
        position       : 'relative',
        boxSizing      : 'border-box',
        width          : '100%',
        borderBottom   : `1px solid ${props.theme.colors.lightGrey}`,
        backgroundColor: props.theme.colors.white,
        textAlign      : 'left',
        fontWeight     : 600,
        pointerEvents  : props.disabled ? 'none' : 'auto'
    }),

    handler: props => ({
        display       : 'inline-block',
        padding       : '0 10px',
        fontSize      : '12px',
        lineHeight    : '36px',
        width         : '100%',
        color         : 'inherit',
        textDecoration: 'none',
        opacity       : props.disabled ? 0.2 : 1,
    }),

    arrow: (props) => ({
        display      : 'block',
        position     : 'absolute',
        right        : '10px',
        top          : '16px',
        zIndex       : 10,
        height       : '0',
        width        : 'auto',
        borderLeft   : '7px solid transparent',
        borderRight  : '7px solid transparent',
        borderTop    : `10px solid ${props.theme.colors.lightGrey}`,
        pointerEvents: 'none',
        transition   : 'transform 0.2s ease-in-out',
        transform    : props.isActive ? 'rotate(-180deg)' : 'none',
    }),

    dropDownList: (props) => ({
        boxSizing      : 'border-box',
        width          : 'calc(100% + 2px)',
        position       : 'absolute',
        padding        : '0 0 5px',
        left           : -1,
        bottom         : '0',
        fontSize       : '12px',
        listStyle      : 'none',
        margin         : 0,
        backgroundColor: props.theme.colors.white,
        border         : `1px solid ${props.theme.colors.lightGrey}`,

        WebkitTransform: 'translateY(100%)',
        msTransform    : 'translateY(100%)',
        transform      : 'translateY(100%)',
        zIndex         : 999,
        overflow       : 'auto',
        maxHeight      : '150px',
    }),

    dropDownItem: (props) => ({
        whiteSpace: 'nowrap',
        padding   : 0,

        '&:hover': {
            backgroundColor: props.theme.colors.turquoise,
            color          : props.theme.colors.white,
        }
    }),

    dropDownLink: {
        padding       : '10px',
        boxSizing     : 'border-box',
        display       : 'inline-block',
        width         : '100%',
        color         : 'inherit',
        textDecoration: 'none',
    },

    nativeSelect: {
        WebkitAppearance: 'menulist-button',
        position        : 'absolute',
        top             : 0,
        left            : 0,
        zIndex          : 99,
        width           : '100%',
        fontSize        : '14px',
        height          : '100%',
        opacity         : 0,
        cursor          : 'pointer'
    }
};
