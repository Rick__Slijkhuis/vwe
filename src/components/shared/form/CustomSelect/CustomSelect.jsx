import React, {Component} from 'react';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';
import glamorous from 'glamorous';

import styles from './styles';

const Select = glamorous.select();
const A = glamorous.a();
const Div = glamorous.div();
const Ul = glamorous.ul();
const Li = glamorous.li();
const Span = glamorous.span();

export default class CustomSelect extends Component {

    /**
     * @type {object}
     */
    static propTypes = {
        defaultValue    : PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.bool]),
        value           : PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.bool]),
        onChange        : PropTypes.func,
        options         : PropTypes.arrayOf(PropTypes.shape({
            value: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.bool]).isRequired,
            label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired
        })).isRequired,
        placeholder     : PropTypes.string,
        getHandler      : PropTypes.func,
        disabled        : PropTypes.bool.isRequired,
        getDropDown     : PropTypes.func,
        customBreakpoint: PropTypes.string,
    };

    static defaultProps = {
        disabled: false
    };

    /**
     * @inheritDoc
     */
    constructor(props) {
        super();

        this.state = {
            value   : props.defaultValue,
            isActive: false
        };
    }

    /**
     * @inheritDoc
     */
    componentWillReceiveProps(newProps) {
        if (typeof this.props.onChange === 'function' && this.props.value !== newProps.value) {
            this.setState(
                () => ({
                    isActive: false,
                    value   : newProps.value
                })
            );
        }
    }

    /**
     * Handle click on select
     *
     * @param {Event} e
     */
    onHandlerClick(e) {
        e.preventDefault();

        this.setState(
            state => ({
                isActive: !state.isActive
            })
        );
    };

    /**
     * Handle click on option
     *
     * @param {string|number|boolean} option
     * @param {Event} e
     */
    onOptionClicked(option, e) {
        e.preventDefault();

        this.onChange(option);
    }

    /**
     * Handle native select change
     *
     * @param {Event} e
     */
    onNativeSelectChange(e) {
        this.onChange(e.target.value);
    }

    /**
     * Perform onChange
     *
     * @param {string|number|boolean} option
     */
    onChange(option) {
        if (typeof this.props.onChange === 'function') {
            this.props.onChange(option);
        } else {
            this.setState(
                () => ({
                    value   : option,
                    isActive: false
                })
            );
        }
    }

    /**
     * Get native select
     *
     * @return {XML}
     */
    getNativeSelect() {
        return (
            <Select onChange={e => this.onNativeSelectChange(e)} value={this.props.value || this.state.value} css={styles.nativeSelect}>
                {this.props.options.map((option, i) => (
                    <option key={i} value={option.value}>{option.label}</option>
                ))}
            </Select>
        )
    }

    /**
     * Get handler
     *
     * @return {XML}
     */
    getHandler() {
        const activeOption = this.props.options.find(option => option.value === (this.props.value || this.state.value));
        const activeOptionLabel = activeOption ? activeOption.label : this.props.placeholder;

        if (typeof this.props.getHandler === 'function') {
            return this.props.getHandler({
                onClick : e => this.onHandlerClick(e),
                children: activeOptionLabel
            });
        }

        return (
            <A href="#" css={themeProps => styles.handler({...themeProps, ...this.props})} onClick={e => this.onHandlerClick(e)}>
                {activeOptionLabel}
                <Span css={styles.arrow} isActive={this.state.isActive}/>
            </A>
        );
    }

    /**
     * Get custom drop down
     *
     * @return {XML}
     */
    getDropDown() {
        const activeOption = this.props.options.find(option => option.value === (this.props.value || this.state.value));

        if (typeof this.props.getDropDown === 'function') {
            return this.props.getDropDown({
                onClick: e => this.onHandlerClick(e),
                activeOption,
                options: {
                    map   : iterator => (
                        this.props.options.map((...args) => (
                            iterator.apply(this, [
                                {
                                    ...args[0],
                                    onClick: e => this.onOptionClicked(args[0].value, e)
                                },
                                ...args.slice(1, args.length)
                            ])
                        ))
                    ),
                    values: this.props.options
                }
            });
        }

        return (
            <Ul css={styles.dropDownList}>
                {this.props.options.map((option, i) => (
                    <Li key={i} css={styles.dropDownItem}>
                        <A css={styles.dropDownLink} href="#" onClick={e => this.onOptionClicked(option.value, e)}>{option.label}</A>
                    </Li>
                ))}
            </Ul>
        )
    }

    render() {

        return (
            <Div css={themeProps => styles.component({...themeProps, ...this.props})}>
                {this.getHandler()}

                {this.props.customBreakpoint
                    ? (
                        <MediaQuery query="only screen and (min-width: 768px)">
                            {matches => (
                                matches ? this.state.isActive && this.getDropDown() : this.getNativeSelect()
                            )}
                        </MediaQuery>
                    ) : (
                        this.getNativeSelect()
                    )
                }

            </Div>
        );
    }

}
