import glamorous from 'glamorous';
import PropTypes from 'prop-types';

const FormRow = glamorous.div((props) => ({
    display      : 'flex',
    flexDirection: 'row',
    flexWrap     : 'wrap',

}), (props) => (props.css));

FormRow.propTypes = {
    css: PropTypes.object,
};

FormRow.defaultProps = {
    css: {},
};

export default FormRow;
