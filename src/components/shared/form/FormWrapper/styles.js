import mq from '../../../../theme/mediaQueries'

export default props => ({
    boxSizing      : 'border-box',
    padding        : props.noPadding ? '0' : '40px 20px',
    margin         : '20px auto',
    width          : '100%',
    height         : props.inScreen ? '70vh' : 'auto',
    border         : `1px solid ${props.theme.colors.grey}`,
    backgroundColor: props.theme.colors.white,

    [`@media ${mq.tablet}`]: {
        margin: ' 30px auto 10px',
    },

    [`@media ${mq.laptop}`]: {
        height : props.inScreen ? '50vh' : 'auto',
        margin : ' 30px auto 10px',
        padding: props.noPadding ? '0' : '40px 30px 40px 0',
    },
});