import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';

import styles from './styles';

const Div = glamorous.div();

const FormWrapper = props => (
    <Div css={[themeProps => styles({...themeProps, ...props}), props.css]}>
        {props.children}
    </Div>
);

FormWrapper.propTypes = {
    inScreen : PropTypes.bool,
    noPadding: PropTypes.bool
};

FormWrapper.defaultProps = {
    inScreen : false,
    noPadding: false
};

export default FormWrapper;
