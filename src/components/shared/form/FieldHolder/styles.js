import mq from '../../../../theme/mediaQueries'

export default {
    main: (props) => {

        let width;

        const prop = props.size;
        const propArray = (props.size.length >= 1) ? prop.split('/') : props.size;
        width = `${propArray[0] / propArray[1] * 100}%`;

        return {
            width       : '100%',
            boxSizing   : 'border-box',
            marginBottom: props.noMarginBottom ? '0' : (props.textAreaMaxLength ? '30px' : '20px'),
            display     : 'inline-block',

            [`@media ${mq.laptop}`]: {
                paddingLeft: '20px',
                width      : width,
            },

            '&:after': {
                content : props.textAreaMaxLength ? `'Max. ${props.textAreaMaxLength}'` : '',
                color   : props.theme.colors.grey,
                fontSize: '12px',
            }
        }
    },

    error: {
        display  : 'block',
        width    : '100%',
        textAlign: 'left',
        color    : 'red',
    }

};