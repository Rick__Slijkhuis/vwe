import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';

import styles from './styles';

const Div = glamorous.div();
const Span = glamorous.span();

const FieldHolder = props => (

    <Div css={[themeProps => styles.main({...themeProps, ...props}), props.css]}>
        {props.children}
        {props.error && (
            <Span css={styles.error}>{props.error}</Span>
        )}
    </Div>
);

FieldHolder.propTypes = {
    css              : PropTypes.object,
    size             : PropTypes.string,
    noMarginBottom   : PropTypes.bool,
    error            : PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    textAreaMaxLength: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
};

FieldHolder.defaultProps = {
    css              : {},
    size             : 'full',
    noMarginBottom   : false,
    error            : false,
    textAreaMaxLength: false,
};

export default FieldHolder;
