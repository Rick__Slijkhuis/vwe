import glamorous from 'glamorous';
import PropTypes from 'prop-types';

const TextInput = glamorous.input((props) => ({
    display: 'inline-block',
    boxSizing: 'border-box',
    width: '100%',
    padding: '10px',
    border: `1px solid ${props.theme.colors.lightGrey}`,
    backgroundColor: props.theme.colors.white,
    color: props.theme.colors.black,
    fontSize: '16px',
    textTransform: 'none',
    cursor: 'auto',
    margin: 0,

    '&:disabled': {
        color: props.theme.colors.lightGrey
    },

    '&:focus': {
        borderColor: props.theme.colors.turquoise
    }

}), (props) => (props.css));

TextInput.propTypes = {
    css: PropTypes.oneOfType([PropTypes.func, PropTypes.object])
};

TextInput.defaultProps = {
    css: {},
};

export default TextInput;
