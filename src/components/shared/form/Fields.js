import glamorous from 'glamorous';
import PropTypes from 'prop-types';
import mq from '../../../theme/mediaQueries'

const Fields = glamorous.div((props) => ({
    display  : 'block',
    width    : '100%',
    textAlign: 'left',

    [`@media ${mq.laptop}`]: {
        display       : 'inline-flex',
        flexWrap      : 'wrap',
        justifyContent: 'space-between',
        width         : '75%',
        textAlign     : 'right',
    },

}), (props) => (props.css));

Fields.propTypes = {
    css: PropTypes.object,
};

Fields.defaultProps = {
    css: {},
};

export default Fields;
