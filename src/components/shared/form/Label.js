import glamorous from 'glamorous';
import PropTypes from 'prop-types';
import mq from '../../../theme/mediaQueries'

const Label = glamorous.p((props) => ({
    display     : 'block',
    width       : '100%',
    paddingTop  : '1em',
    marginBottom: '1em',
    textAlign   : 'left',
    lineHeight  : '1',

    [`@media ${mq.laptop}`]: {
        display  : 'inline-block',
        width    : '23%',
        textAlign: 'right',
    },

}), (props) => (props.css));

Label.propTypes = {
    css: PropTypes.object,
};

Label.defaultProps = {
    css: {},
};

export default Label;
