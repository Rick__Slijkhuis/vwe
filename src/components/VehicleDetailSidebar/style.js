export default {
    container: props => ({
        display     : 'block',
        width       : '100%',
    }),

    navWrapper: props => ({
        display        : 'none',
        backgroundColor: props.theme.colors.darkGrey,

        [`@media ${props.theme.mediaQueries.tablet}`]: {
            display: 'block',
        },
    }),

    image: props => ({
        display     : 'block',
        width       : '100%',
        marginBottom: '20px',
    }),

    heading: props => ({
        boxSizing   : 'border-box',
        padding     : '0 20px',
        lineHeight  : '70px',
        margin      : 0,
        color       : props.theme.colors.white,
        borderBottom: `1px solid ${props.theme.colors.offWhite}`
    }),

    nav: props => ({
        listStyle: 'none',
        margin   : 0,
        padding  : 0,
    }),

    navItem: props => ({
        display     : 'block',
        margin      : 0,
        padding     : 0,
        borderBottom: `1px solid ${props.theme.colors.offWhite}`
    }),

    navLink: props => ({
        display       : 'block',
        boxSizing     : 'border-box',
        textDecoration: 'none',
        lineHeight    : '50px',
        padding       : '0 20px',
        color         : props.theme.colors.white,
        transition    : 'background 0.15s ease-out',

        '&:hover': {
            backgroundColor: props.theme.colors.turquoise
        }
    }),
}

