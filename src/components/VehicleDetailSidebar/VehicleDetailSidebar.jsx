import React, {Component} from 'react';
import glamorous from 'glamorous';

import style from './style'

const Div = glamorous.div();
const Img = glamorous.img();
const H3 = glamorous.h3();
const Ul = glamorous.ul();
const Li = glamorous.li();
const A = glamorous.a();

class VehicleDetail extends Component {

    render() {

        return (
            <Div css={style.container}>
                <Img css={style.image} src={this.props.vehicleImage || '/images/car_placeholder.jpg'}/>

                <Div css={style.navWrapper}>
                    <H3 css={style.heading}>Snel naar</H3>

                    <Ul css={style.nav}>
                        <Li css={style.navItem}>
                            <A css={style.navLink} href="#general">Basisgegevens</A>
                        </Li>
                        <Li css={style.navItem}>
                            <A css={style.navLink} href="#engine">Motor en Techniek</A>
                        </Li>
                        <Li css={style.navItem}>
                            <A css={style.navLink} href="#size">Afmetingen en gewicht</A>
                        </Li>
                        <Li css={style.navItem}>
                            <A css={style.navLink} href="#finance">Financieel</A>
                        </Li>
                    </Ul>
                </Div>
            </Div>
        )
    }
}

export default VehicleDetail;
