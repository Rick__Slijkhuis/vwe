import React, {Component, createElement} from 'react';
import glamorous from 'glamorous';
import moment from 'moment';
import Link from 'react-router-dom/Link';

import Category from './Category';
import Table from '../shared/Table';

import style from './style'

const Div = glamorous.div();
const H2 = glamorous.h2();
const Span = glamorous.span();
const A = glamorous.a();
const I = glamorous.i();

const backLink = props => createElement(Link, props);

class VehicleDetail extends Component {

    render() {

        const BackLinkElement = glamorous(backLink)();

        const vehicle = this.props.vehicle.toJS();
        const {registration, engineConfiguration, weights, dimensions, finance} = vehicle;

        return (
            <Div css={style.container}>
                <Div css={style.heading}>
                    <Span css={style.registrationNumber}>{registration.registrationNumber}</Span>
                    <H2 css={style.makeAndModel}>{vehicle.make} {vehicle.model}</H2>

                    <Span css={style.headingOptions}>
                        <BackLinkElement css={style.Button} to="/">
                            <I css={style.buttonIcon} className="fa fa-angle-left"/>
                            Terug naar lijst
                        </BackLinkElement>
                        <A css={style.Button} href="">
                            <I css={style.buttonIcon} className="fa fa-print"/>
                            Printen
                        </A>
                    </Span>
                </Div>

                <Category title="Basisgegevens" id="general">
                    <Table items={[
                        {key: 'Categorie', value: vehicle.vehicleCategory},
                        {key: 'Type', value: vehicle.vehicleType},
                    ]}/>
                    <Table items={[
                        {key: 'Vervalsdatum APK', value: moment(registration.motExpiryDate).format('DD-MM-YYYY')},
                        {key: 'Datum van invoering', value: moment(vehicle.dateOfFirstAdmissionNetherlands).format('DD-MM-YYYY')},
                    ]}/>
                </Category>

                <Category title="Motor en techniek" id="engine">
                    <Table items={[
                        {key: 'Brandstoftype', value: engineConfiguration.engines[0].fuelType},
                        {key: 'Transmissie', value: engineConfiguration.gearbox},
                        {key: 'Versnellingen', value: engineConfiguration.numberOfGears},
                      ]}/>

                    <Table items={[
                        {key: 'Verbruik', value: `${engineConfiguration.engines[0].consumption.average}l / 100km`},
                        {key: 'in de stad', value: `${engineConfiguration.engines[0].consumption.urban}l / 100km`},
                        {key: 'op snelweg', value: `${engineConfiguration.engines[0].consumption.extraUrban}l / 100km`},
                    ]}/>
                </Category>

                <Category title="Afmetingen en gewicht" id="size">
                    <Table items={[
                        {key: 'Breedte', value: dimensions.width || 'niet beschikbaar'},
                        {key: 'Lengte', value: dimensions.height || 'niet beschikbaar'},
                    ]}/>

                    <Table items={[
                        {key: 'gewicht', value: `${weights.deadWeight}kg`},
                        {key: 'gewicht casco', value: `${weights.kerbWeight}kg`},
                    ]}/>
                </Category>

                <Category title="Financiën" id="finance">
                    <Table items={[
                        {key: 'Catalogusprijs', value: `€ ${finance.cataloguePrice},-` || 'niet beschikbaar'},
                        {key: 'Netto BPM', value:  `€ ${finance.grossBpm},-` || 'niet beschikbaar'},
                    ]}/>
                </Category>
            </Div>
        )
    }
}

export default VehicleDetail;
