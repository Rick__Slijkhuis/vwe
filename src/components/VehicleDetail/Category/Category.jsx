import React, {Component} from 'react';
import glamorous from 'glamorous';

import style from './style'

const H3 = glamorous.h3();
const Div = glamorous.div();

class Category extends Component {

    render() {

        return (
            <Div css={style.category} id={this.props.id}>
                <H3 css={style.heading}>
                    {this.props.title}
                </H3>
                {this.props.children}
            </Div>
        )
    }
}

export default Category;
