export default {
    category: props => ({
        marginBottom: '40px'
    }),

    heading: props => ({
        fontSize: '18px',
    }),
}

