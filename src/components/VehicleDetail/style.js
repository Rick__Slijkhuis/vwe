export default {

    container: props => ({
        width    : '100%',
        boxSizing: 'border-box',
    }),

    heading: props => ({
        fontSize    : '15px',
        lineHeight  : '24px',
        marginBottom: '40px',
        borderBottom: `1px solid ${props.theme.colors.lightGrey}`
    }),

    registrationNumber: props => ({
        ...props.theme.fonts.bodyBold,
        display        : 'inline-block',
        backgroundColor: props.theme.colors.licensePlate,
        textTransform  : 'uppercase',
        fontSize       : '16px',
        lineHeight     : '30px',
        padding        : '0 15px',
        marginRight    : 20,
        borderRadius   : 3
    }),

    makeAndModel: props => ({
        ...props.theme.fonts.bodyBold,
        display   : 'inline-block',
        fontSize  : '18px',
        lineHeight: '30px',
        margin    : '0 0 20px 0',
        padding   : 0,
    }),

    headingOptions: props => ({
        display: 'inline-block',
        float  : 'right',
        color  : props.theme.colors.black
    }),

    Button: props => ({
        display       : 'none',
        textDecoration: 'none',
        fontSize      : '15px',
        lineHeight    : '30px',
        marginRight   : 20,
        color         : props.theme.colors.black,


        [`@media ${props.theme.mediaQueries.laptop}`]: {
            display: 'inline-block',
        },
    }),

    buttonIcon: props => ({
        display    : 'inline-block',
        fontSize   : '18px',
        lineHeight : '30px',
        marginRight: 7,
        color      : props.theme.colors.black
    }),
}

