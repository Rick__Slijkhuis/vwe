import React, {Component} from 'react';
import glamorous from 'glamorous';

import VehicleListItem from './VehicleListItem';

import style from './style'

const Ul = glamorous.ul();

class Vehicles extends Component {

    render() {

        const vehicles = this.props.vehicles.toArray();

        return (
            <Ul css={style.list}>
                {vehicles.map((vehicle, index) => (
                    <VehicleListItem key={index} vehicle={vehicle}/>
                ))}
            </Ul>
        )
    }
}

export default Vehicles;
