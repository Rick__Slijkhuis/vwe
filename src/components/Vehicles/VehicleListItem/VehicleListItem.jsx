import React, {Component, createElement} from 'react';
import glamorous from 'glamorous';
import moment from 'moment';
import Link from 'react-router-dom/Link';

import Table from '../../shared/Table';

import style from './style'

const Li = glamorous.li();
const Div = glamorous.div();
const Img = glamorous.img();
const H2 = glamorous.h2();
const Span = glamorous.span();

const VehicleLink = props => createElement(Link, props);

class VehicleListItem extends Component {

    render() {

        const VehicleLinkElement = glamorous(VehicleLink)();

        const vehicle = this.props.vehicle;

        const gearboxes = {
            A: 'Automaat',
            C: 'Elektrisch',
            H: 'Handgeschakeld'
        };

        return (
            <Li css={style.vehicle}>

                <VehicleLinkElement css={style.link} to={`${vehicle.get('registrationNumber')}`}>

                    <Div css={style.imageWrapper}>
                        <Img css={style.image} src={vehicle.get('image') || '/images/car_placeholder.jpg'}/>
                    </Div>

                    <Div css={style.content}>
                        <Div css={themeProps => style.heading}>
                            <Span css={style.registrationNumber}>{vehicle.get('registrationNumber')}</Span>
                            <H2 css={style.makeAndModel}>{vehicle.get('make')} {vehicle.get('model')}</H2>
                        </Div>

                        <Div css={style.tables}>
                            <Table items={[
                                {key: 'Type', value: vehicle.get('vehicleCategory')},
                                {key: 'Transmissie', value: gearboxes[vehicle.get('gearbox')]},
                            ]}/>

                            <Table items={[
                                {key: 'Datum eerste toelating', value: moment(vehicle.get('dateOfFirstAdmissionNetherlands')).format('DD-MM-YYYY')},
                                {key: 'Vervaldatum APK', value: moment(vehicle.get('motExpiryDate')).format('DD-MM-YYYY')},
                            ]}/>
                        </Div>
                    </Div>
                </VehicleLinkElement>
            </Li>
        )
    }
}

export default VehicleListItem;
