export default {
    vehicle: props => ({
        display     : 'block',
        boxSizing   : 'border-box',
        width       : '100%',
        listStyle   : 'none',
        marginBottom: 20,
    }),

    link: props => ({
        display        : 'block',
        width          : '100%',
        textDecoration : 'none',
        color          : props.theme.colors.black,
        transition     : 'background 0.15s ease-out',
        borderBottom   : `1px solid ${props.theme.colors.lightGrey}`,
        backgroundColor: props.theme.colors.white,

        '&:hover': {
            backgroundColor: props.theme.colors.nearlyWhite
        },

        [`@media ${props.theme.mediaQueries.tablet}`]: {
            display: 'flex',
        },
    }),

    imageWrapper: props => ({

        [`@media ${props.theme.mediaQueries.tablet}`]: {
            width: 280,
        },
    }),

    image: props => ({
        display: 'block',
        width  : '100%',
    }),

    content: props => ({
        flex   : 1,
        padding: 20,
    }),

    registrationNumber: props => ({
        ...props.theme.fonts.bodyBold,
        display        : 'inline-block',
        backgroundColor: props.theme.colors.licensePlate,
        textTransform  : 'uppercase',
        fontSize       : '16px',
        lineHeight     : '30px',
        padding        : '0 15px',
        marginRight    : 20,
        borderRadius   : 3
    }),

    makeAndModel: props => ({
        ...props.theme.fonts.bodyBold,
        display   : 'inline-block',
        fontSize  : '18px',
        lineHeight: '30px',
        margin    : '0 0 20px 0',
        padding   : 0,
    }),

    heading: props => ({
        fontSize    : '15px',
        lineHeight  : '24px',
        marginBottom: '20px'
    }),

    tables: props => ({
        display: 'block'
    })
}

