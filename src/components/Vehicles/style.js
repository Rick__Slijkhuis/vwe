export default {
    list: props => ({
        display  : 'block',
        listStyle: 'none',
        width    : '100%',
        padding  : 0,
        margin   : 0,
    }),
}

