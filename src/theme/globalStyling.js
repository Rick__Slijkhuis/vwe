import {css} from 'glamor';
import fonts from './fonts';

css.global('html, body', {
    ...fonts.bodyRegular,
    WebkitFontSmoothing: 'antialiased',
    '& *' : {
        boxSizing: 'border-box',
    }
});

