export default {
    smallMobile: 'only screen and (max-width: 350px)',
    smallTablet: 'only screen and (min-width: 600px)',
    tablet     : 'only screen and (min-width: 768px)',
    laptop     : 'only screen and (min-width: 1024px)',
    desktop    : 'only screen and (min-width: 1200px)'
}
