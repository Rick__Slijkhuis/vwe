export default {
    bodyRegular: {
        fontFamily: "'Open Sans',sans-serif",
        fontWeight: 400,
    },

    bodyBold: {
        fontFamily: "'Open Sans',sans-serif",
        fontWeight: 700,
    },
};
