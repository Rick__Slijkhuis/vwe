import colors from './colors';
import fonts from './fonts';
import mediaQueries from './mediaQueries';
import spacing from './spacing';
import eases from './eases';

export default {
    colors,
    fonts,
    mediaQueries,
    spacing,
    eases,
}
