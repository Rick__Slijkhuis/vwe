import React, {Component} from 'react';
import Switch from 'react-router-dom/Switch';
import Route from 'react-router-dom/Route';

import './theme/globalStyling';

import Header from './components/Header';
import Home from './containers/Home';
import Vehicle from './containers/Vehicle';

class App extends Component {

    render() {

        return (
            <span>
                <Header/>

                <Switch location={this.props.location}>
                    <Route path="/" exact component={Home}/>
                    <Route path="/:vehicleId" exact component={Vehicle}/>
                </Switch>
            </span>
        );
    }
}

export default App;
