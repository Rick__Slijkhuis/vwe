export default {
    background: props => ({
        backgroundColor: props.theme.colors.offWhite,
    }),

    container: props => ({
        display  : 'block',
        maxWidth : 1200,
        margin: '0 auto',
        padding  : '40px 20px',
        boxSizing: 'border-box',

        [`@media ${props.theme.mediaQueries.laptop}`]: {
            display  : 'flex',
        },
    })
}

