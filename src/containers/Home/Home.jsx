import React, {Component} from 'react';
import glamorous from 'glamorous';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import style from './style'

import {fetchVehiclesData, updateVehiclesList} from '../../actions/vehiclesActions';

import Sidebar from '../../components/shared/Sidebar';
import ContentContainer from '../../components/shared/ContentContainer';
import Vehicles from '../../components/Vehicles';
import VehicleOrdering from '../../components/VehicleOrdering';
import VehicleFiltering from '../../components/VehicleFiltering';

const Div = glamorous.div();

class Home extends Component {

    componentDidMount() {
        this.props.fetchVehiclesData();
    }

    getFilteredVehicles() {

        const activeFilters = this.props.filters.filter(filter => {
            return filter;
        });

        if(activeFilters.size === 0) {
            return this.props.vehicles;
        }

        const filteredVehicles = this.props.vehicles.filter(vehicle => {
            
            if ((activeFilters.get('model') && activeFilters.get('make')) &&
                activeFilters.get('model') === vehicle.get('model') &&
                activeFilters.get('make') === vehicle.get('make')) {
                return true
            }

            if ((activeFilters.get('make') && !activeFilters.get('model')) &&
                activeFilters.get('make') === vehicle.get('make')) {
                return true
            }
            
            if (
                (activeFilters.get('licencePlate')) &&
                vehicle.get('registrationNumber').indexOf(activeFilters.get('licencePlate')) !== -1
            ) {
                return true
            }
        });

        return filteredVehicles;
    }

    render() {
        return (
            <Div css={style.background}>
                <Div css={style.container}>

                    {this.props.vehicles &&
                        <Sidebar flex={true}>
                            <VehicleFiltering vehicles={this.props.vehicles}
                                              filters={this.props.filters}
                                              updateVehiclesList={this.props.updateVehiclesList}/>
                        </Sidebar>
                    }

                    {this.props.vehicles &&
                        <ContentContainer flex={true} home={true}>

                            <VehicleOrdering/>

                            <Vehicles vehicles={this.getFilteredVehicles()}/>

                        </ContentContainer>
                    }
                </Div>
            </Div>
        )
    }
}

export default connect(
    state => ({
        vehicles: state.vehicles.get('data'),
        filters: state.vehicles.get('filters'),
    }),
    dispatch => bindActionCreators({
        fetchVehiclesData,
        updateVehiclesList,
    }, dispatch)
)(Home);
