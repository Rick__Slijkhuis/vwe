export default {
    container: props => ({
        display: 'block',
        maxWidth : 1200,
        margin   : '0 auto',
        padding  : 30,
        boxSizing: 'border-box',

        [`@media ${props.theme.mediaQueries.tablet}`]: {
            display  : 'flex',
        },
    })
}

