import React, {Component} from 'react';
import glamorous from 'glamorous';
import MediaQuery from 'react-responsive';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import style from './style'

import {fetchVehicleData} from '../../actions/vehicleActions';

import Sidebar from '../../components/shared/Sidebar';
import ContentContainer from '../../components/shared/ContentContainer';

import VehicleDetail from '../../components/VehicleDetail';
import VehicleDetailSidebar from '../../components/VehicleDetailSidebar';

const Div = glamorous.div();

class Vehicle extends Component {

    componentDidMount() {

        this.props.fetchVehicleData(this.props.match.params.vehicleId);
    }

    render() {

        return (
            <Div css={style.container}>

                <Sidebar flex={true}>
                    {this.props.vehicle &&
                        <VehicleDetailSidebar vehicleImage={this.props.vehicle.get('image')}/>
                    }
                </Sidebar>

                <ContentContainer flex={true}>
                    {this.props.vehicle &&
                        <VehicleDetail vehicle={this.props.vehicle}/>
                    }
                </ContentContainer>
            </Div>
        )
    }
}


export default connect(
    state => ({
        vehicle: state.vehicle.get('data')
    }),
    dispatch => bindActionCreators({
        fetchVehicleData
    }, dispatch)
)(Vehicle);
