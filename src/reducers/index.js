import {combineReducers} from 'redux';
import vehicles from './vehicles';
import vehicle from './vehicle';

export default combineReducers({
    vehicles,
    vehicle,
});
