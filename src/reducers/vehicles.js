import {fromJS} from 'immutable';

import {
    VEHICLES_LOADING,
    VEHICLES_FETCH_DATA_SUCCESS,
    VEHICLES_FETCH_DATA_ERROR,
    VEHICLES_UPDATE_LIST,
} from '../constants/actionTypes';

const initialState = fromJS({
    loading: false,
    data   : {},
    filters : {
        model       : null,
        make        : null,
        licencePlate: null
    },
    errors : null
});

export default function tutorReducer(state = initialState, action) {
    switch (action.type) {
        case VEHICLES_LOADING:
            return state.set('loading', true);
        case VEHICLES_FETCH_DATA_SUCCESS:
            return state.set('data', fromJS(action.payload)).set('loading', false);
        case VEHICLES_FETCH_DATA_ERROR:
            return state.set('loading', false).set('errors', action.payload);
        case VEHICLES_UPDATE_LIST:
            return state.setIn([`filters`, action.payload.key], action.payload.value);
        default:
            return state;
    }
}
