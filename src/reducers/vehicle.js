import {fromJS} from 'immutable';

import {
    VEHICLE_LOADING,
    VEHICLE_FETCH_DATA_SUCCESS,
    VEHICLE_FETCH_DATA_ERROR,
} from '../constants/actionTypes';

const initialState = fromJS({
    loading: false,
    data     : null,
    errors   : null
});

export default function tutorReducer(state = initialState, action) {
    switch (action.type) {
        case VEHICLE_LOADING:
            return state.set('loading', true);
        case VEHICLE_FETCH_DATA_SUCCESS:
            return state.set('data', fromJS(action.payload)).set('loading', false);
        case VEHICLE_FETCH_DATA_ERROR:
            return state.set('loading', false).set('errors', action.payload);
        default:
            return state;
    }
}
