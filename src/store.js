import {createStore, applyMiddleware, compose} from 'redux';
import thunkMiddleware from 'redux-thunk';
import reducers from './reducers/index';

let createStoreWithMiddleware;

// apply middleware to redux createStore
if (process.env.NODE_ENV === 'development') {
    // var createLogger = require('redux-logger');
    createStoreWithMiddleware = compose(
        applyMiddleware(thunkMiddleware),
        (window.devToolsExtension && process.env.NODE_ENV === 'development') ? window.devToolsExtension() : f => f
    )(createStore);
} else {
    createStoreWithMiddleware = applyMiddleware(thunkMiddleware)(createStore);
}

// create redux store
const store = createStoreWithMiddleware(reducers);

export default store;
