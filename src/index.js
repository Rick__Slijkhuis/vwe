import React from 'react';
import Router from 'react-router-dom/BrowserRouter';
import Route from 'react-router-dom/Route';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {ThemeProvider} from 'glamorous';

import store from './store';
import theme from './theme';
import App from './App';

ReactDOM.render(
    <Provider store={store}>
        <ThemeProvider theme={theme}>
            <Router>
                <Route path={`/:vehicleId?`} component={App}/>
            </Router>
        </ThemeProvider>
    </Provider>,
    document.getElementById('root')
);
