import axios from 'axios';

import {
    VEHICLES_LOADING,
    VEHICLES_FETCH_DATA_SUCCESS,
    VEHICLES_FETCH_DATA_ERROR,
    VEHICLES_UPDATE_LIST,
} from '../constants/actionTypes';

/**
 * Create VEHICLES_SAVE_LOADING action
 *
 * @return {{type: string}}
 */
function loading() {
    return {type: VEHICLES_LOADING};
}

/**
 * Save tutor profile image action
 *
 * @return {function(*, *)}
 */
export function fetchVehiclesData() {
    return (dispatch) => {

        dispatch(loading());

        return axios.get(`https://vwe.herokuapp.com/api/vehicles`).then(
            response => dispatch(fetchVehiclesDataSuccess(response.data)),
            error => dispatch(fetchVehiclesDataError(error.response.data))
        )
    }
}

/**
 * Create VEHICLES_FETCH_DATA_SUCCESS action
 *
 * @param {Array} payload
 * @return {{type: string, payload: Array}}
 */
export function fetchVehiclesDataSuccess(payload) {
    return {type: VEHICLES_FETCH_DATA_SUCCESS, payload};
}

/**
 * Create VEHICLES_FETCH_DATA_ERROR action
 *
 * @param {*} payload
 * @return {{type: string, error: *}}
 */
function fetchVehiclesDataError(payload) {
    return {type: VEHICLES_FETCH_DATA_ERROR, payload};
}

/**
 * Create VEHICLES_UPDATE_LIST action
 *
 * @param {String} key
 * @param {String} value
 * @return {{type: string, payload: Object}}
 */
export function updateVehiclesList(key, value) {
    return {type: VEHICLES_UPDATE_LIST, payload:{key: key, value: value}};
}
