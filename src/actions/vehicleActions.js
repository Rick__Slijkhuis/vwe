import axios from 'axios';

import {
    VEHICLE_LOADING,
    VEHICLE_FETCH_DATA_SUCCESS,
    VEHICLE_FETCH_DATA_ERROR,
} from '../constants/actionTypes';

/**
 * Create VEHICLE_SAVE_LOADING action
 *
 * @return {{type: string}}
 */
function loading() {
    return {type: VEHICLE_LOADING};
}

/**
 * Save tutor profile image action
 *
 * @return {function(*, *)}
 */
export function fetchVehicleData(vehicleId) {
    return (dispatch) => {

        dispatch(loading());

        return axios.get(`https://vwe.herokuapp.com/api/vehicle/${vehicleId}`).then(
            response => dispatch(fetchVehicleDataSuccess(response.data)),
            error => dispatch(fetchVehicleDataError(error.response.data))
        )
    }
}

/**
 * Create VEHICLE_FETCH_DATA_SUCCESS action
 *
 * @param {Array} payload
 * @return {{type: string, payload: Array}}
 */
export function fetchVehicleDataSuccess(payload) {
    return {type: VEHICLE_FETCH_DATA_SUCCESS, payload};
}

/**
 * Create VEHICLE_FETCH_DATA_ERROR action
 *
 * @param {*} payload
 * @return {{type: string, error: *}}
 */
function fetchVehicleDataError(payload) {
    return {type: VEHICLE_FETCH_DATA_ERROR, payload};
}
